# Gentoo Bootstrap

## setup_build_gentoo.sh

Optional script. Does the security group and key pair setup that is in build_gentoo.sh. Done so keys and security group can be set up just once.

can pass 4 options, in order:

1. region
2. security group
3. key pair
4. private key of key pair

if you do not set these:

* region defaults to `us-east-1`
* security group defaults to `gentoo-bootstrap` and it will be created if needed
* key pair defaults to "gentoo-bootstrap\_`$region`" e.g. `gentoo-bootstrap_us-east-1`
* key file defaults to "gentoo-bootstrap\_`$region`.pem" e.g. `gentoo-bootstrap_us-east-1.pem`
* The key pair will be created if needed.

Sample command to setup us-east-1

```bash
./setup_build_gentoo.sh us-east-1
```

## build_gentoo.sh

The pain of keeping the 32 and 64 bit versions in sync finally outweight the pain of merging the two scripts together. This and `remote_gentoo.sh` (as opposed to `i386/remote_gentoo.sh` and `x86_64/remote_gentoo.sh`) are the result of that.

Builds Gentoo image for architecture given. Will use a c1.medium for bootstrap instance.

Will use a t1.micro as test instance.

Because sudo will be called, this script cannot be run in the backgroud.

Recommend running screen before running.

Takes 5 options:

1. architecture
2. region 
3. security group
4. key pair
5. keyfile

if you do not set these:

* architecture is not optional. The script will give an error. Valid values are: `i386`, `x86_64`
* region defaults to `us-east-1`
* security group defaults to `gentoo-bootstrap` and it will be created if needed
* key pair defaults to "gentoo-bootstrap\_`$region`-`uuidgen`" e.g. `gentoo-bootstrap_us-east-1-78917775-1adc-40b9-9fba-bc84b7158683`
* key file defaults to "gentoo-bootstrap\_`$region`-`uuidgen`.pem" e.g. `gentoo-bootstrap_us-east-1-78917775-1adc-40b9-9fba-bc84b7158683.pem`
* key pair will be created if needed.

Default AMI name is Gentoo\_64-bit-EBS-`date +%Y-%m-%d-%H-%M-%S` if architecture is `x86_64` and is Gentoo\_32-bit-EBS-`date +%Y-%m-%d-%H-%M-%S` if architecture is `i386`, but can be changed if the environment variable `AMI_NAME` is set.

Sample command for building 64 bit gentoo on us-east-1, using all defaults

```bash
./build_gentoo.sh x86_64
```

Sample command for building 64 bit gentoo on us-east-1

```bash
./build_gentoo.sh x86_64 us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

Sample command for building 32 bit gentoo on us-east-1

```bash
./build_gentoo.sh i386 us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

## build_gentoo_64.sh

Wrapper for `build_gentoo.sh`, builds the regular 64-bit Gentoo image.

Because sudo will be called, this script cannot be run in the backgroud.

Recommend running screen before running.

Architecture is set for you, but the other 4 options remain:

1. region 
2. security group
3. key pair
4. keyfile

Sample command for building 64 bit gentoo on us-east-1

```bash
./build_gentoo_64.sh us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

## build_gentoo_32.sh

Wrapper for `build_gentoo.sh`, builds the regular 32-bit Gentoo image.

Because sudo will be called, this script cannot be run in the backgroud.

Recommend running screen before running.

Architecture is set for you, but the other 4 options remain:

1. region 
2. security group
3. key pair
4. keyfile

Sample command for building 32 bit gentoo on us-east-1

```bash
./build_gentoo_32.sh us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

## build_minimal_gentoo_64.sh

Wrapper for `build_gentoo.sh`, builds the minimal 64-bit Gentoo image. See the AMI_TYPE for more about minimal build.

Because sudo will be called, this script cannot be run in the backgroud.

Recommend running screen before running.

Architecture is set for you, but the other 4 options remain:

1. region 
2. security group
3. key pair
4. keyfile

Default AMI name is minimal-Gentoo\_64-bit-EBS-`date +%Y-%m-%d-%H-%M-%S`, but can be changed if the environment variable `AMI_NAME` is set.

Sample command for building 64 bit gentoo on us-east-1

```bash
./build_minimal_gentoo_64.sh
```

See the AMI_TYPE section below for more information on this AMI.

## build_minimal_gentoo_32.sh

Wrapper for `build_gentoo.sh`, builds the minimal 64-bit Gentoo image. See the AMI_TYPE for more about minimal build.

Because sudo will be called, this script cannot be run in the backgroud.

Recommend running screen before running.

Architecture is set for you, but the other 4 options remain:

1. region 
2. security group
3. key pair
4. keyfile

Default AMI name is minimal-Gentoo\_32-bit-EBS-`date +%Y-%m-%d-%H-%M-%S`, but can be changed if the environment variable `AMI_NAME` is set.

Sample command for building 32 bit gentoo on us-east-1

```bash
./build_minimal_gentoo_32.sh
```

## remote_gentoo.sh

The script to copied to and executed on bootstrap instance.

## x86_64/

The contents of this directory will be copied to `/tmp` on the instance building the 64-bit gentoo image.

## i386/

The contents of this directory will be copied to `/tmp` on the instance building the 32-bit gentoo image.

## x86_64/.config & i386/.config

The .config file for building the kernel.

## make_latest_push.sh

Makes the latest gentoo AMI public.

Takes 3 options:

1. region
2. architecture
3. root_device_type

Sample command for making us-east-1 64-bit EBS image public

```bash
./make_latest_public.sh us-east-1 x86_64 ebs
```

Sample command for making us-east-1 32-bit EBS image public

```bash
./make_latest_public.sh us-east-1 i386 ebs
```

Sample command for making us-east-1 64-bit Instance Store image public

```bash
./make_latest_public.sh us-east-1 x86_64 instance-store
```

## copy_ebs_across_region.sh

Copies latest public EBS gentoo image across regions.

Takes 3 options:

1. from_region
2. architecture
3. to_region

Sample command for copying 64-bit image from us-east-1 to us-west-2

```bash
./copy_ebs_across_region.sh us-east-1 x86_64 us-west-2
```

Sample command for copying 32-bit image from us-east-1 to us-west-2

```bash
./copy_ebs_across_region.sh us-east-1 i386 us-west-2
```

# AMI_TYPE

Using the `AMI_TYPE` environment variable, variants of the AMI can be built. Currently there is only one value that does anything:

* minimal

## minimal

Will setup a minimal AMI.

The only items in `/var/lib/portage/world` are `app-admin/sudo` and `sys-kernel/gentoo-sources`

# Setup across all regions

Need to setup only once.

```bash
./setup_build_gentoo.sh us-east-1
```

Building can be run in parallel to save time.

```bash
./build_gentoo_64.sh us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

```bash
./build_gentoo_32.sh us-east-1 gentoo-bootstrap gentoo-bootstrap_us-east-1 gentoo-bootstrap_us-east-1.pem
```

Copy across regions

```bash
./make_latest_public.sh us-east-1 x86_64 ebs
./make_latest_public.sh us-east-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 us-west-2
./copy_ebs_across_region.sh us-east-1 i386 us-west-2
./make_latest_public.sh us-west-2 x86_64 ebs
./make_latest_public.sh us-west-2 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 us-west-1
./copy_ebs_across_region.sh us-east-1 i386 us-west-1
./make_latest_public.sh us-west-1 x86_64 ebs
./make_latest_public.sh us-west-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 eu-west-1
./copy_ebs_across_region.sh us-east-1 i386 eu-west-1
./make_latest_public.sh eu-west-1 x86_64 ebs
./make_latest_public.sh eu-west-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 sa-east-1
./copy_ebs_across_region.sh us-east-1 i386 sa-east-1
./make_latest_public.sh sa-east-1 x86_64 ebs
./make_latest_public.sh sa-east-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-northeast-1
./copy_ebs_across_region.sh us-east-1 i386 ap-northeast-1
./make_latest_public.sh ap-northeast-1 x86_64 ebs
./make_latest_public.sh ap-northeast-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-southeast-1
./copy_ebs_across_region.sh us-east-1 i386 ap-southeast-1
./make_latest_public.sh ap-southeast-1 x86_64 ebs
./make_latest_public.sh ap-southeast-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-southeast-2
./copy_ebs_across_region.sh us-east-1 i386 ap-southeast-2
./make_latest_public.sh ap-southeast-2 x86_64 ebs
./make_latest_public.sh ap-southeast-2 i386 ebs
```
