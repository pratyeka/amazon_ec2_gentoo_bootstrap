#!/bin/bash

region=$1
architecture=$2
root_device_type=$3

if [ $architecture == "x86_64" -a $root_device_type == "ebs" ]; then
    building="Gentoo 64 EBS"
elif [ $architecture == "i386" -a $root_device_type == "ebs" ]; then
	building="Gentoo 32 EBS"
elif [ $architecture == "x86_64" -a $root_device_type == "instance-store" ]; then
    building="Gentoo 64 Instance-Store"
elif [ $architecture == "i386" -a $root_device_type == "instance-store" ]; then
	building="Gentoo 32 Instance-Store" 
else
	echo "Unknown architecture, root_device_type combination"
	echo "architecture = $architecture"
	echo "root_device_type = $root_device_type"
	echo "valid values for architecture: x86_64, i386"
	echo "valid values for root_device_type: ebs, instance-store"
	exit
fi

start_time=`date +%Y-%m-%dT%H:%M:%S`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: AMI_TYPE = $AMI_TYPE"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: region = $region"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: architecture = $architecture"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: root_device_type = $root_device_type"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding latest image"

if [ -z $AMI_TYPE ]; then
    manifest_location="*/Gentoo_*"
else
    manifest_location="*/{$AMI_TYPE}-Gentoo_*"
fi

latest_image=`ec2-describe-images \
--region $region \
--owner self \
--filter image-type=machine \
--filter manifest-location=${manifest_location} \
--filter architecture=$architecture \
--filter root-device-type=$root_device_type \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| head -n 1`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: latest_image = $latest_image"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: checking if public"

public_check=`echo $latest_image | awk '{ print $6 }'`

if [[ $public_check == "public" ]]; then
	echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: already public"
	exit
else
	echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: is private"
fi

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding ami"

ami=`echo $latest_image | awk '{ print $2 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: ami = $ami"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: making public"
ec2-modify-image-attribute --region $region $ami --launch-permission --add all
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: done"

