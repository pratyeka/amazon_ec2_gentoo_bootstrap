#!/bin/bash

from_region=$1
architecture=$2
to_region=$3

if [[ $architecture == "x86_64" ]]; then
    if [ -z $AMI_TYPE ]; then
        building="Gentoo 64 EBS"
    	ami_description="Gentoo 64-bit EBS"
    else
        building="Gentoo 64 EBS ($AMI_TYPE)"
    	ami_description="$AMI_TYPE Gentoo 64-bit EBS"
    fi
elif [[ $architecture == "i386" ]]; then
    if [ -z $AMI_TYPE ]; then
        building="Gentoo 32 EBS"
    	ami_description="Gentoo 32-bit EBS"
    else
        building="Gentoo 32 EBS ($AMI_TYPE)"
    	ami_description="$AMI_TYPE Gentoo 32-bit EBS"
    fi
else
	echo "Unknown architecture: $architecture"
	echo "valid values for architecture: x86_64, i386"
	exit
fi

start_time=`date +%Y-%m-%dT%H:%M:%S`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: AMI_TYPE = $AMI_TYPE"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: from_region = $from_region"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: architecture = $architecture"
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: to_region = $to_region"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding latest kernel-id"

latest_kernel=`ec2-describe-images \
--region $to_region \
--filter image-type=kernel \
--filter manifest-location=*pv-grub* \
--owner amazon \
--filter architecture=$architecture \
| grep -v "hd00" \
| awk '{ print $3 "\t"  $2 }' \
| sed "s:.*/pv-grub-hd0[^0-9]*::" \
| sort \
| tail -n 1 \
| awk '{ print $2 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: latest_kernel = $latest_kernel"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding latest image"

if [ -z $AMI_TYPE ]; then
    manifest_location="*/Gentoo_*"
else
    manifest_location="*/{$AMI_TYPE}-Gentoo_*"
fi

latest_image=`ec2-describe-images \
--region $from_region \
--owner self \
--filter image-type=machine \
--filter is-public=true \
--filter manifest-location=${manifest_location} \
--filter architecture=$architecture \
--filter root-device-type=ebs \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| head -n 1`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: latest_image = $latest_image"


echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding owner"
OWNER=`echo $latest_image | sed -e "s/\/.*//g"`
echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: owner = $OWNER"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding from ami"

from_ami=`echo $latest_image | awk '{ print $2 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: from_ami = $from_ami"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding name"

name=`echo $latest_image | awk '{ print $3 }' | sed -e "s/${OWNER}\///g"`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: name = $name"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding from snapshot"

from_snapshot=`ec2-describe-images \
--region $from_region \
$from_ami \
| grep /dev/sda1 \
| awk '{ print $4 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: from_snapshot = $from_snapshot"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: finding description"

description=`ec2-describe-snapshots --region $from_region $from_snapshot | awk '{ print $9 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: description = $description"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: copying snapshot"

to_snapshot=`ec2-copy-snapshot \
--region $to_region \
--source-region $from_region \
--source-snapshot-id $from_snapshot \
--description $description \
| awk '{ print $2 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: to_snapshot = $to_snapshot"

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: checking if snapshot is done"

completed_check=0
while [ $completed_check -eq 0 ]; do
    sleep 60
    echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: checking if snapshot is done (1 minute check)"
    let completed_check=`ec2-describe-snapshots \
        --region $to_region \
        $to_snapshot \
        --filter status=completed \
        | wc -c`
done

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: snapshot is done"

new_image=`ec2-register \
--region $to_region \
--name $name \
--description "$ami_description" \
--architecture $architecture \
--kernel $latest_kernel \
--root-device-name /dev/sda1 \
--block-device-mapping "/dev/sda1=$to_snapshot" \
--block-device-mapping "/dev/sdb=ephemeral0" \
--block-device-mapping "/dev/sdc=ephemeral1" \
--block-device-mapping "/dev/sdd=ephemeral2" \
--block-device-mapping "/dev/sde=ephemeral3" \
| awk '{ print $2 }'`

echo "$building $start_time - `date +%Y-%m-%dT%H:%M:%S`: new_image = $new_image"

