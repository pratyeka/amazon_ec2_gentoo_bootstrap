#!/bin/bash

OWNER="902460189751"

function current_image
{
ec2-describe-images \
--region $1 \
--owner $OWNER \
--filter image-type=machine \
--filter is-public=true \
--filter manifest-location=*/Gentoo_* \
--filter architecture=$3 \
--filter root-device-type=$2 \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| awk '{ print "<tr><td>$REGION</td><td><a href=\"https://console.aws.amazon.com/ec2/home?region=$REGION#launchAmi="$2"\">"$2"</a></td><td>"$7"</td><td>"$10"</td><td>"$9"</td><td>"$3"</td></tr>" }' \
| sed -e "s/<td>[^<]*\/Gentoo/<td>Gentoo/g" \
| sed -e "s/\$REGION/$1/g" \
| head -n 1
}

function current_minimal_image
{
ec2-describe-images \
--region $1 \
--owner $OWNER \
--filter image-type=machine \
--filter is-public=true \
--filter manifest-location=*/minimal-Gentoo_* \
--filter architecture=$3 \
--filter root-device-type=$2 \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| awk '{ print "<tr><td>$REGION</td><td><a href=\"https://console.aws.amazon.com/ec2/home?region=$REGION#launchAmi="$2"\">"$2"</a></td><td>"$7"</td><td>"$10"</td><td>"$9"</td><td>"$3"</td></tr>" }' \
| sed -e "s/<td>[^<]*\/Gentoo/<td>Gentoo/g" \
| sed -e "s/\$REGION/$1/g" \
| head -n 1
}

function old_images
{
ec2-describe-images \
--region $1 \
--owner $OWNER \
--filter image-type=machine \
--filter is-public=true \
--filter manifest-location=*/Gentoo_* \
--filter architecture=$3 \
--filter root-device-type=$2 \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| awk '{ print "<tr><td>$REGION</td><td><a href=\"https://console.aws.amazon.com/ec2/home?region=$REGION#launchAmi="$2"\">"$2"</a></td><td>"$7"</td><td>"$10"</td><td>"$9"</td><td>"$3"</td></tr>" }' \
| sed -e "s/<td>[^<]*\/Gentoo/<td>Gentoo/g" \
| sed -e "s/\$REGION/$1/g" \
| tail -n +2
}

function old_minimal_images
{
ec2-describe-images \
--region $1 \
--owner $OWNER \
--filter image-type=machine \
--filter is-public=true \
--filter manifest-location=*/minimal-Gentoo_* \
--filter architecture=$3 \
--filter root-device-type=$2 \
| grep "^IMAGE" \
| awk '{ print $3 $0 }' \
| sort -r \
| awk '{ print "<tr><td>$REGION</td><td><a href=\"https://console.aws.amazon.com/ec2/home?region=$REGION#launchAmi="$2"\">"$2"</a></td><td>"$7"</td><td>"$10"</td><td>"$9"</td><td>"$3"</td></tr>" }' \
| sed -e "s/<td>[^<]*\/Gentoo/<td>Gentoo/g" \
| sed -e "s/\$REGION/$1/g" \
| tail -n +2
}

function start_current_images
{
echo "<table>"
echo "<thead>"
echo "<tr><th>Region</th><th>Image ID</th><th>Architecture</th><th>Root Device Type</th><th>Kernel ID</th><th>Name</th></tr>"
echo "</thead>"
echo "<tbody>"
}

function end_current_images
{
echo "</tbody>"
echo "</table>"
}

function start_current_minimal_images
{
echo "<hr>"
echo "<p>Minimal Images</p>"
echo "<table>"
echo "<thead>"
echo "<tr><th>Region</th><th>Image ID</th><th>Architecture</th><th>Root Device Type</th><th>Kernel ID</th><th>Name</th></tr>"
echo "</thead>"
echo "<tbody>"
}

function end_current_minimal_images
{
echo "</tbody>"
echo "</table>"
}

function start_old_images
{
echo "<hr>"
echo "<p>Old Images</p>"
echo "<table>"
echo "<thead>"
echo "<tr><th>Region</th><th>Image ID</th><th>Architecture</th><th>Root Device Type</th><th>Kernel ID</th><th>Name</th></tr>"
echo "</thead>"
echo "<tbody>"
}

function end_old_images
{
echo "</tbody>"
echo "</table>"
}

function start_old_minimal_images
{
echo "<hr>"
echo "<p>Old Minimal Images</p>"
echo "<table>"
echo "<thead>"
echo "<tr><th>Region</th><th>Image ID</th><th>Architecture</th><th>Root Device Type</th><th>Kernel ID</th><th>Name</th></tr>"
echo "</thead>"
echo "<tbody>"
}

function end_old_minimal_images
{
echo "</tbody>"
echo "</table>"
}

function intro
{
echo "<p>Dowd and Associates is proud to offer <a href=\"http://www.gentoo.org/\">Gentoo Linux</a> images on Amazon's EC2 Service.</p>"
echo "<!--break-->"
echo "<p>The username to login with these instances is <code>ec2-user</code></p>"
echo "<p>If you have the EC2 command line tools installed, you can start an instance like so:</p>"
echo "<pre><samp><var>user</var>@<var>hostname</var> ~ $ <kbd>ec2-run-instances ami-8b66afe2 -k <var>key-name</var></kbd></samp></pre>"
echo "<p>To login, you would do:</p>"
echo "<pre><samp><var>user</var>@<var>hostname</var> ~ $ <kbd>ssh -i <var>\$HOME/.ssh/key_name</var> ec2-user@<var>ec2-1-2-3-4.compute-1.amazonaws.com</var></kbd></samp></pre>"
}

intro

start_current_images

current_image "ap-northeast-1" "ebs" "i386"
current_image "ap-northeast-1" "ebs" "x86_64"
current_image "ap-northeast-1" "instance-store" "i386"
current_image "ap-northeast-1" "instance-store" "x86_64"

current_image "ap-southeast-1" "ebs" "i386"
current_image "ap-southeast-1" "ebs" "x86_64"
current_image "ap-southeast-1" "instance-store" "i386"
current_image "ap-southeast-1" "instance-store" "x86_64"

current_image "ap-southeast-2" "ebs" "i386"
current_image "ap-southeast-2" "ebs" "x86_64"
current_image "ap-southeast-2" "instance-store" "i386"
current_image "ap-southeast-2" "instance-store" "x86_64"

current_image "eu-west-1" "ebs" "i386"
current_image "eu-west-1" "ebs" "x86_64"
current_image "eu-west-1" "instance-store" "i386"
current_image "eu-west-1" "instance-store" "x86_64"

current_image "sa-east-1" "ebs" "i386"
current_image "sa-east-1" "ebs" "x86_64"
current_image "sa-east-1" "instance-store" "i386"
current_image "sa-east-1" "instance-store" "x86_64"

current_image "us-east-1" "ebs" "i386"
current_image "us-east-1" "ebs" "x86_64"
current_image "us-east-1" "instance-store" "i386"
current_image "us-east-1" "instance-store" "x86_64"

current_image "us-west-1" "ebs" "i386"
current_image "us-west-1" "ebs" "x86_64"
current_image "us-west-1" "instance-store" "i386"
current_image "us-west-1" "instance-store" "x86_64"

current_image "us-west-2" "ebs" "i386"
current_image "us-west-2" "ebs" "x86_64"
current_image "us-west-2" "instance-store" "i386"
current_image "us-west-2" "instance-store" "x86_64"

end_current_images

start_current_minmal_images

current_minimal_image "ap-northeast-1" "ebs" "i386"
current_minimal_image "ap-northeast-1" "ebs" "x86_64"
current_minimal_image "ap-northeast-1" "instance-store" "i386"
current_minimal_image "ap-northeast-1" "instance-store" "x86_64"

current_minimal_image "ap-southeast-1" "ebs" "i386"
current_minimal_image "ap-southeast-1" "ebs" "x86_64"
current_minimal_image "ap-southeast-1" "instance-store" "i386"
current_minimal_image "ap-southeast-1" "instance-store" "x86_64"

current_minimal_image "ap-southeast-2" "ebs" "i386"
current_minimal_image "ap-southeast-2" "ebs" "x86_64"
current_minimal_image "ap-southeast-2" "instance-store" "i386"
current_minimal_image "ap-southeast-2" "instance-store" "x86_64"

current_minimal_image "eu-west-1" "ebs" "i386"
current_minimal_image "eu-west-1" "ebs" "x86_64"
current_minimal_image "eu-west-1" "instance-store" "i386"
current_minimal_image "eu-west-1" "instance-store" "x86_64"

current_minimal_image "sa-east-1" "ebs" "i386"
current_minimal_image "sa-east-1" "ebs" "x86_64"
current_minimal_image "sa-east-1" "instance-store" "i386"
current_minimal_image "sa-east-1" "instance-store" "x86_64"

current_minimal_image "us-east-1" "ebs" "i386"
current_minimal_image "us-east-1" "ebs" "x86_64"
current_minimal_image "us-east-1" "instance-store" "i386"
current_minimal_image "us-east-1" "instance-store" "x86_64"

current_minimal_image "us-west-1" "ebs" "i386"
current_minimal_image "us-west-1" "ebs" "x86_64"
current_minimal_image "us-west-1" "instance-store" "i386"
current_minimal_image "us-west-1" "instance-store" "x86_64"

current_minimal_image "us-west-2" "ebs" "i386"
current_minimal_image "us-west-2" "ebs" "x86_64"
current_minimal_image "us-west-2" "instance-store" "i386"
current_minimal_image "us-west-2" "instance-store" "x86_64"

end_current_minimal_images

start_old_images

old_images "ap-northeast-1" "ebs" "i386"
old_images "ap-northeast-1" "ebs" "x86_64"
old_images "ap-northeast-1" "instance-store" "i386"
old_images "ap-northeast-1" "instance-store" "x86_64"

old_images "ap-southeast-1" "ebs" "i386"
old_images "ap-southeast-1" "ebs" "x86_64"
old_images "ap-southeast-1" "instance-store" "i386"
old_images "ap-southeast-1" "instance-store" "x86_64"

old_images "ap-southeast-2" "ebs" "i386"
old_images "ap-southeast-2" "ebs" "x86_64"
old_images "ap-southeast-2" "instance-store" "i386"
old_images "ap-southeast-2" "instance-store" "x86_64"

old_images "eu-west-1" "ebs" "i386"
old_images "eu-west-1" "ebs" "x86_64"
old_images "eu-west-1" "instance-store" "i386"
old_images "eu-west-1" "instance-store" "x86_64"

old_images "sa-east-1" "ebs" "i386"
old_images "sa-east-1" "ebs" "x86_64"
old_images "sa-east-1" "instance-store" "i386"
old_images "sa-east-1" "instance-store" "x86_64"

old_images "us-east-1" "ebs" "i386"
old_images "us-east-1" "ebs" "x86_64"
old_images "us-east-1" "instance-store" "i386"
old_images "us-east-1" "instance-store" "x86_64"

old_images "us-west-1" "ebs" "i386"
old_images "us-west-1" "ebs" "x86_64"
old_images "us-west-1" "instance-store" "i386"
old_images "us-west-1" "instance-store" "x86_64"

old_images "us-west-2" "ebs" "i386"
old_images "us-west-2" "ebs" "x86_64"
old_images "us-west-2" "instance-store" "i386"
old_images "us-west-2" "instance-store" "x86_64"

end_old_images

start_old_minimal_images

old_minimal_images "ap-northeast-1" "ebs" "i386"
old_minimal_images "ap-northeast-1" "ebs" "x86_64"
old_minimal_images "ap-northeast-1" "instance-store" "i386"
old_minimal_images "ap-northeast-1" "instance-store" "x86_64"

old_minimal_images "ap-southeast-1" "ebs" "i386"
old_minimal_images "ap-southeast-1" "ebs" "x86_64"
old_minimal_images "ap-southeast-1" "instance-store" "i386"
old_minimal_images "ap-southeast-1" "instance-store" "x86_64"

old_minimal_images "ap-southeast-2" "ebs" "i386"
old_minimal_images "ap-southeast-2" "ebs" "x86_64"
old_minimal_images "ap-southeast-2" "instance-store" "i386"
old_minimal_images "ap-southeast-2" "instance-store" "x86_64"

old_minimal_images "eu-west-1" "ebs" "i386"
old_minimal_images "eu-west-1" "ebs" "x86_64"
old_minimal_images "eu-west-1" "instance-store" "i386"
old_minimal_images "eu-west-1" "instance-store" "x86_64"

old_minimal_images "sa-east-1" "ebs" "i386"
old_minimal_images "sa-east-1" "ebs" "x86_64"
old_minimal_images "sa-east-1" "instance-store" "i386"
old_minimal_images "sa-east-1" "instance-store" "x86_64"

old_minimal_images "us-east-1" "ebs" "i386"
old_minimal_images "us-east-1" "ebs" "x86_64"
old_minimal_images "us-east-1" "instance-store" "i386"
old_minimal_images "us-east-1" "instance-store" "x86_64"

old_minimal_images "us-west-1" "ebs" "i386"
old_minimal_images "us-west-1" "ebs" "x86_64"
old_minimal_images "us-west-1" "instance-store" "i386"
old_minimal_images "us-west-1" "instance-store" "x86_64"

old_minimal_images "us-west-2" "ebs" "i386"
old_minimal_images "us-west-2" "ebs" "x86_64"
old_minimal_images "us-west-2" "instance-store" "i386"
old_minimal_images "us-west-2" "instance-store" "x86_64"

end_old_minimal_images

